from silf.backend.commons.util.config import validate_mandatory_config_keys
from silf.backend.commons.device import *
from silf.drivers.light_relay.relay import *


class RelayDevice(Device):
    MAIN_LOOP_INTERVAL = 1

    def __init__(self, device_id="default", config_file=None):
        super().__init__(device_id, config_file)
        mandatory_config = ['gpio',]

        if self.conifg is None:
            raise AssertionError("Config file required")

        validate_mandatory_config_keys(self.config, device_id, mandatory_config)

        if self.config.getboolean( device_id, "use_mock", fallback = False):
            self.relay_driver = MockDriver()
        else:
            self.relay_driver = RelayDriver( int (self.config[device_id]['gpio']) )

    def post_power_up_diagnostics(self, diagnostics_level = DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]):
        if not self.relay_driver.is_open():
            raise DiagnosticsException("Driver is closed - should be open")

    def apply_settings(self, settings):

        relay_is_on = self.__get_setting(settings, "relayState", "defaultRelayState")

        if relay_is_on:
            self.relay_driver.on()
        else:
            self.relay_driver.off()    

    def __get_setting(self, settings, settings_key, config_default_key):
        if settings_key in settings:
            return settings[settings_key]
        if config_default_key in self.config[self.device_id]:
            return self.config.getfloat(self.device_id, config_default_key)
        raise DeviceException(self.__NO_SETTING_ERROR.format( settings_key, config_default_key, settings ) )

    def pop_results(self):
        return [ { 'relayState' : self.relay_driver.is_on } ]

    def start(self):
        super().start()
    
    def stop():
        super().stop()
        self.relay_driver.off()

    def _tearDown(self):
        self.relay_driver.close()

    __NO_SETTING_ERROR = (
        "Setting {} is missing both from settings dictionary and default "
        "configuration (should be under key {}). Settings dict was "
        "(for your reference): {}"
    )
