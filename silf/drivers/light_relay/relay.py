from time import sleep

class RelayDriver(object):

    def __init__(self, gpioNumber):
        self.gpioNumber = gpioNumber
        self.relay_is_on = False
        self.is_open = False
        try:
            export = open("/sys/class/gpio/export", "w")
            export.write(str(self.gpioNumber))
            export.close()
        except OSError as err:
            print("Export gpio"+str(self.gpioNumber)+" probably already created - ignoring. OSError: ", err)
        
        sleep(0.2) #Maybe not needed?
        direction_file = "/sys/class/gpio/gpio"+str(self.gpioNumber)+"/direction"      
        try:
            direction = open(direction_file, "w")
            direction.write("out")
            direction.close()
        except OSError as err:
            print("Could not change gpio"+str(self.gpioNumber)+" for out ", err)

        try:
            self.relay = open("/sys/class/gpio/gpio"+str(self.gpioNumber)+"/value","w")
            self.relay.write("1")
            self.relay.flush()
            self.is_open=True
        except OSError as err:
            print("Relay error (off): ", err)

    def on(self):
        if not self.is_open:
            return
        try:
            self.relay.write("0")
            self.relay.flush()
            self.relay_is_on = True
        except OSError as err:
            print("Relay error (on): ", err)

    def off(self):
        if not self.is_open:
            return
        try:
            self.relay.write("1")
            self.relay.flush()
            self.relay_is_on = False
        except OSError as err:
            print("Relay error (off): ", err)

    @property
    def is_on(self):
        return self.relay_is_on

    def close(self):        
        try:
            unexport = open("/sys/class/gpio/unexport","w")
            unexport.write(str(self.gpioNumber))
            unexport.close()
        except OSError as err:
            print("Cannot unexport gpio"+str(self.gpioNumber)+" - ignoring. OSError: ", err)
        else:
            print("gpio"+str(self.gpioNumber)+" unexported successfuly.")
        self.is_open=False
