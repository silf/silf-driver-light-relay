import time

class MockRelayDriver(object):

    def __init__(self, gpioNumber):
        self.gpioNumber = gpioNumber
        self.relay_is_on = False
        self.is_open = True
        time.sleep(0.2)  #

    def on(self):
        self.relay_is_on = True

    def off(self):
        self.relay_is_on = False

    @property
    def is_on(self):
        return self.relay_is_on

    def close(self):
        self.is_open = False


