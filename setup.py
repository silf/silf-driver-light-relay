#!/usr/bin/env python3
# coding=utf-8

from distutils.core import setup
from configparser import ConfigParser

from os import path
from pip.req import parse_requirements

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

try:
    from pip.download import PipSession
    pip_session = PipSession()
    install_reqs = parse_requirements(
        path.join(path.dirname(__file__), 'REQUIREMENTS'),
        session=pip_session)
except ImportError:
    install_reqs = parse_requirements(
        path.join(path.dirname(__file__), 'REQUIREMENTS'))

if __name__ == "__main__":

    cp = ConfigParser()
    file = path.join(path.dirname(__file__), "silf", "drivers", "light_relay", "version.ini")
    with open(file) as f:
        cp.read_file(f)

    setup(name='silf-backend-driver-light-relay',
          description="Light switching relay module for Raspberry Pi ",
          version=cp['VERSION']['version'],
          packages=[
              'silf.drivers.light_relay',
              ],
          package_data = {
              'silf.drivers.light_relay': ['version.ini']
          },
          url='',
          license='',
          author='Dariusz Tefelski',
          author_email='tefelski@if.pw.edu.pl',
          install_requires = [str(ir.req) for ir in install_reqs]
    )
